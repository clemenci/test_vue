module.exports = {
    pages: {
        index: {
            entry: 'src/main.js',
            title: 'Vue test'
        }
    },
    publicPath: process.env.NODE_ENV === 'production' ?
        '' : '/'
}
